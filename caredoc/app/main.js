import Vue from "nativescript-vue";
import Main from "./components/Main";
import store from "./store";
import VueDevtools from "nativescript-vue-devtools";
import { localize } from "nativescript-localize";

if (TNS_ENV !== "production") {
  Vue.use(VueDevtools);
}
// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = TNS_ENV === "production";
Vue.filter("L", localize);
Vue.filter("numberWithComma", val => {
  let retVal = typeof val !== "string" ? val + "" : val;
  return retVal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
});

new Vue({
  store,
  render: h => h("frame", [h(Main)])
}).$start();
